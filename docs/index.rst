===============
Penta
===============
Penta is a pure python shell program that i have written to improve my understanding of python.i am working on it actively for further improvements.

Here is a list of Commands that are supported:

1:cd -------- change directories

2:list --------- list content of directory

3:cls/clr/clear ----------------- clear the console

4:read/type/cat ---------------- Read files

5:sysinfo/systeminfo ------------ shows system information

6:rem --------------- remove files and folders

7:ren/rename ------------------------ rename files and folders

8:mve/move -------------------------- Move files and folders

9:cpy --------------------- copy files and directories

10:mdir ------------------- Create directories

11:title --------------------- Change title of the shell

12:dime -------------------- Prints Current time and date

13:username ------------------- shows Currently logged Username

14:create -------------------- Create files
 
15:ping-host -------------------- Ping an host.

16:configip --------------------- Shows network info

17:getip ----------------------- prints Ipaddress

18:getmac --------------------- prints Mac address

19:murder ---------------------- kills an process

20:shown ---------------------- shutdown ,reboot,logout system

21:chp ------------------------ Change file permission

22:os ver /os version ----------------------- Prints OS info

23:reveal/search --------------------------- Search Files

24:ver/version ------------------------ Shows Penta version

25:help ----------------------------- Shows an Help table

26:chistory ------------------------- Shows Commands history

27:about -------------------------- About Penta

28:gtube---------------------------- A little utility to download videos and playlists from youtube

29:troute------------------------------------tracerouting.

More comming soon

For more info visit:github.com/Justaus3r/Penta
